package com.chernov.loganalyzer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LogAnalyzerBackendApplication

fun main(args: Array<String>) {
    runApplication<LogAnalyzerBackendApplication>(*args)
}
